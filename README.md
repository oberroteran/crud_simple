# REPOSITORIO MOVIDO
Este proyecto se ha movido a: 
https://github.com/berroteran/java_simple_crud

# README #

Cada x punto en el tiempo alguien quiere iniciarse en el mundo JAVA y crear alguna aplicación webDinamica, tal vez esto pueda ayudarte.

Creado hace algún tiempo lo comparto para quien sea útil.

### What is this repository for? ###
Es un ejemplo de como usar java para una aplicacion web.

Características:

* Pure HTML, JavaScript, DHTML.
* No FrameWork,
* POO no bien implementada, mas POP(procedimientos) para agilizar el desarrollo.
* Soporte a Multi Motores de base de datos: H2, sqlite, mysql, SQLServer y postgres.
* Maven



### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact